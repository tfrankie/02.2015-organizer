package clientorg;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class AddNotePanel extends JPanel implements ActionListener{
	public JButton add;
	public JButton cancel;
	public JLabel ddmmyyyy;
	public JLabel hhmm;
	public JTextArea text;
	private Font font;
	private JButton nextd,prevd,nextm,prevm,nexty,prevy,nexth,prevh,nextmin,prevmin;
	public Calendar owncalendar;
	
	private void Init(){
		setSize(360,320);
		setLocation(0,0);	
		setLayout(null);
		font = new Font("TimesRoman", Font.PLAIN, 18);
		add = new JButton("Dodaj");
		cancel = new JButton("Anuluj");
		ddmmyyyy= new JLabel();
		hhmm = new JLabel();
		text= new JTextArea();
		nextd = new JButton(new ImageIcon(getClass().getResource("nextdmy.png")));
		prevd = new JButton(new ImageIcon(getClass().getResource("prevdmy.png")));
		nextm = new JButton(new ImageIcon(getClass().getResource("nextdmy.png")));
		prevm = new JButton(new ImageIcon(getClass().getResource("prevdmy.png")));
		nexty = new JButton(new ImageIcon(getClass().getResource("nextdmy.png")));
		prevy = new JButton(new ImageIcon(getClass().getResource("prevdmy.png")));
		nexth = new JButton(new ImageIcon(getClass().getResource("nextdmy.png")));
		prevh = new JButton(new ImageIcon(getClass().getResource("prevdmy.png")));
		nextmin = new JButton(new ImageIcon(getClass().getResource("nextdmy.png")));
		prevmin = new JButton(new ImageIcon(getClass().getResource("prevdmy.png")));
		nextd.addActionListener(this);
		prevd.addActionListener(this);
		nextm.addActionListener(this);
		prevm.addActionListener(this);
		nexty.addActionListener(this);
		prevy.addActionListener(this);
		nexth.addActionListener(this);
		prevh.addActionListener(this);
		nextmin.addActionListener(this);
		prevmin.addActionListener(this);
		nextd.setBounds(90,3,20,20);
		prevd.setBounds(90,43,20,20);
		nextm.setBounds(116,3,20,20);
		prevm.setBounds(116,43,20,20);
		nexty.setBounds(150,3,20,20);
		prevh.setBounds(220,43,20,20);
		nexth.setBounds(220,3,20,20);
		prevmin.setBounds(245,43,20,20);
		nextmin.setBounds(245,3,20,20);
		prevy.setBounds(150,43,20,20);
		add.setBounds(70,245,100,20);
		cancel.setBounds(190,245,100,20);
		ddmmyyyy.setBounds(90,23,100,20);
		ddmmyyyy.setFont(font);
		hhmm.setBounds(220,23,60,20);
		hhmm.setFont(font);
		JScrollPane scrollPane = new JScrollPane(text);
		scrollPane.setBounds(37,70,280,160);
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		add(prevd);
		add(nextd);
		add(prevm);
		add(nextm);
		add(prevy);
		add(nexty);
		add(prevh);
		add(nexth);
		add(prevmin);
		add(nextmin);
		add(ddmmyyyy);
		add(hhmm);
		add(scrollPane);
		add(add);
		add(cancel);
	}
	
	AddNotePanel(){
		Init();
		owncalendar = Calendar.getInstance();
	}

	public void setDate(Calendar calendar){
		Calendar present = Calendar.getInstance();
		owncalendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE),present.get(Calendar.HOUR_OF_DAY),present.get(Calendar.MINUTE));
		ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + (((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR))));
		hhmm.setText(((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));
	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source==prevd){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE)-1);
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
			}
		if(source==nextd){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE)+1);
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
		}		
		if(source==prevm){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH)-1, owncalendar.get(Calendar.DATE));
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
		}
		if(source==nextm){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH)+1, owncalendar.get(Calendar.DATE));
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
		}
		if(source==prevy){
			owncalendar.set(owncalendar.get(Calendar.YEAR)-1, owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE));
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
		}
		if(source==nexty){
			owncalendar.set(owncalendar.get(Calendar.YEAR)+1, owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE));
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
		}

		if(source==prevh){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE), owncalendar.get(Calendar.HOUR_OF_DAY)-1, owncalendar.get(Calendar.MINUTE));
			hhmm.setText(((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));
		}
		if(source==nexth){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE), owncalendar.get(Calendar.HOUR_OF_DAY)+1, owncalendar.get(Calendar.MINUTE));
			hhmm.setText(((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));

		}
		if(source==prevmin){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE), owncalendar.get(Calendar.HOUR_OF_DAY), owncalendar.get(Calendar.MINUTE)-1);
			hhmm.setText(((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));

		}
		if(source==nextmin){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE), owncalendar.get(Calendar.HOUR_OF_DAY), owncalendar.get(Calendar.MINUTE)+1);
			hhmm.setText(((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));

		}
	}
}
