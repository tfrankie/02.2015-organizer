package clientorg;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;


public class MainPanel extends JPanel implements ActionListener {
	public JButton Menu;
	public JButton Rok;
	public JButton Miesiac;
	public JButton Dzien;
	public JButton Dzisiaj;
	public JButton Prev;
	public JButton Next;
	public Month month;
	public Year year;
	public Day day;
	public Month othermonth;
	public Day otherday;
	public Year otheryear;
	private Calendar calendar;
	public Calendar cnextprev;
	public ArrayList<Note> notelist;
	
	private void Init(){
		setVisible(true);
		setSize(360,300);
		setLayout(null);
		//USTAWIENIE BUTTONOW MENU, ROK, MIESIAC, DZIEN, DZISIAJ
		Menu = new JButton(new ImageIcon(getClass().getResource("menu.png")));
		Menu.setBounds(0,0,34,25);
		Dzisiaj = new JButton(new ImageIcon(getClass().getResource("today.png")));
		Dzisiaj.setBounds(75,0,60,25);
		Dzien = new JButton(new ImageIcon(getClass().getResource("day.png")));
		Dzien.setBounds(173,0,60,25);
		Miesiac = new JButton(new ImageIcon(getClass().getResource("month.png")));
		Miesiac.setBounds(233,0,60,25);
		Rok = new JButton(new ImageIcon(getClass().getResource("year.png")));
		Rok.setBounds(293,0,60,25);
		Prev = new JButton(new ImageIcon(getClass().getResource("prev.png")));
		Prev.setBounds(50,0,25,25);
		Next = new JButton(new ImageIcon(getClass().getResource("next.png")));
		Next.setBounds(135,0,25,25);
		add(Menu);
		add(Dzisiaj);
		add(Dzien);
		add(Miesiac);
		add(Rok);
		add(Next);
		add(Prev);
		Dzien.addActionListener(this);
		Miesiac.addActionListener(this);
		Rok.addActionListener(this);
		Dzisiaj.addActionListener(this);
		Prev.addActionListener(this);
		Next.addActionListener(this);
		
	}
	
	public MainPanel(Calendar calendar, ArrayList<Note> list) {
		Init();
		this.calendar=calendar;
		notelist = list;
		cnextprev = Calendar.getInstance();
		//PO WEJSCIU WIDOK MONTH
		year=new Year(calendar);
		month= new Month(calendar);
		day = new Day(calendar);
		othermonth= new Month(calendar);
		otherday = new Day(calendar);
		otheryear = new Year(calendar);
		add(month);
		Miesiac.setEnabled(false);
		add(day);
		add(year);
		add(othermonth);
		add(otherday);
		add(otheryear);
		day.setVisible(false);
		year.setVisible(false);
		othermonth.setVisible(false);
		otherday.setVisible(false);
		otheryear.setVisible(false);
		}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source=e.getSource();
		if(source==Dzisiaj){
			if(!Dzien.isEnabled()){
				day.setVisible(true);
				otherday.setVisible(false);
				cnextprev.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
				otherday.refresh(notelist);
				day.refresh(notelist);
			}
			else if(!Miesiac.isEnabled()){
				month.setVisible(true);
				othermonth.setVisible(false);
				cnextprev.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
				othermonth.refresh(notelist);
				month.refresh(notelist);
			}
			else if(!Rok.isEnabled()){
				year.setVisible(true);
				otheryear.setVisible(false);
				cnextprev.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
			}
			else System.out.println("BLAD");
		}
		if(source==Dzien){
			day.setVisible(true);
			year.setVisible(false);
			month.setVisible(false);
			Rok.setEnabled(true);
			Miesiac.setEnabled(true);
			Dzien.setEnabled(false);
			otherday.setVisible(false);
			othermonth.setVisible(false);
			otheryear.setVisible(false);
			cnextprev.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
			day.refresh(notelist);
		}
		if(source==Miesiac){
			day.setVisible(false);
			year.setVisible(false);
			month.setVisible(true);
			Miesiac.setEnabled(false);
			Dzien.setEnabled(true);
			Rok.setEnabled(true);
			otherday.setVisible(false);
			othermonth.setVisible(false);
			otheryear.setVisible(false);
			cnextprev.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
			month.refresh(notelist);
		}
		if(source==Rok){
			day.setVisible(false);
			year.setVisible(true);
			month.setVisible(false);
			Rok.setEnabled(false);
			Miesiac.setEnabled(true);
			Dzien.setEnabled(true);
			otherday.setVisible(false);
			othermonth.setVisible(false);
			otheryear.setVisible(false);
			cnextprev.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
		}
		if(source==Prev && !Miesiac.isEnabled()){
			othermonth.setVisible(false);
			remove(othermonth);
			cnextprev.set(cnextprev.get(Calendar.YEAR), cnextprev.get(Calendar.MONTH)-1, cnextprev.get(Calendar.DATE));
	    	othermonth=new Month(cnextprev);
			othermonth.refresh(notelist);
			add(othermonth);
	    	month.setVisible(false);
	    	othermonth.setVisible(true);
		}
		
		if(source==Next && !Miesiac.isEnabled()){
			
			othermonth.setVisible(false);
			remove(othermonth);
			cnextprev.set(cnextprev.get(Calendar.YEAR), cnextprev.get(Calendar.MONTH)+1, cnextprev.get(Calendar.DATE));
			othermonth=new Month(cnextprev);
			othermonth.refresh(notelist);
			add(othermonth);
			month.setVisible(false);
			othermonth.setVisible(true);	
		}
		if(source==Prev && !Dzien.isEnabled()){
			otherday.setVisible(false);
			remove(otherday);
			cnextprev.set(cnextprev.get(Calendar.YEAR), cnextprev.get(Calendar.MONTH), cnextprev.get(Calendar.DATE)-1);
			otherday=new Day(cnextprev);
			otherday.refresh(notelist);
			add(otherday);
			day.setVisible(false);
			otherday.setVisible(true);
		}

		if(source==Next && !Dzien.isEnabled()){
			otherday.setVisible(false);
			remove(otherday);
			cnextprev.set(cnextprev.get(Calendar.YEAR), cnextprev.get(Calendar.MONTH), cnextprev.get(Calendar.DATE)+1);
			otherday=new Day(cnextprev);
			otherday.refresh(notelist);
			add(otherday);
			day.setVisible(false);
			otherday.setVisible(true);
		}
		if(source==Prev && !Rok.isEnabled()){
			otheryear.setVisible(false);
			remove(otheryear);
			cnextprev.set(cnextprev.get(Calendar.YEAR)-1, cnextprev.get(Calendar.MONTH), cnextprev.get(Calendar.DATE));
			otheryear=new Year(cnextprev);
			add(otheryear);
			year.setVisible(false);
			otheryear.setVisible(true);
		}

		if(source==Next && !Rok.isEnabled()){
			otheryear.setVisible(false);
			remove(otheryear);
			cnextprev.set(cnextprev.get(Calendar.YEAR)+1, cnextprev.get(Calendar.MONTH), cnextprev.get(Calendar.DATE));
			otheryear=new Year(cnextprev);
			add(otheryear);
			year.setVisible(false);
			otheryear.setVisible(true);
		}
	}
}