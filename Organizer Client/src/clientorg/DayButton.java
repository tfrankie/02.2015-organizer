package clientorg;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.util.Calendar;
import java.util.LinkedList;

public class DayButton extends JButton {
	static int height= 38;
	static int width= 50;
	private int MonthDay;
	private int notescount;
	
	public DayButton(int day, int x, int y, boolean present){
		super(Integer.toString(day),null);
		MonthDay = day;
		setBounds(x,y,width,height);
		setVisible(true);
		setContentAreaFilled(present);
	    setVerticalTextPosition(SwingConstants.TOP);
	    setHorizontalTextPosition(SwingConstants.CENTER);
	    notescount=0;
	    }
	
	public void addNotesCount(){
		notescount++;
		if(notescount == 1)
			setIcon(new ImageIcon(getClass().getResource("noteinmonth1.png")));
		else if(notescount == 2)
			setIcon(new ImageIcon(getClass().getResource("noteinmonth2.png")));
		else if(notescount > 2)
			setIcon(new ImageIcon(getClass().getResource("noteinmonth3.png")));
	}
	public void resetNotesCount(){
		notescount=0;
		setIcon(null);
	}
	
}
