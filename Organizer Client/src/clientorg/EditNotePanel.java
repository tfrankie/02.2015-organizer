package clientorg;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;



public class EditNotePanel extends JPanel implements ActionListener{
	
	public JButton save,cancel;
	private JButton nextd,prevd,nextm,prevm,nexty,prevy,nexth,prevh,nextmin,prevmin;
	public JLabel ddmmyyyy;
	public JLabel hhmm;
	public JTextArea text;
	public Calendar owncalendar;
	public Font font;
	private Note presentnote;
	private String copyddmmyyyy, copyhhmm;
	private Calendar copycal;
	
	public void keepnote(Note note){
		copycal.set(note.cal.get(Calendar.YEAR), note.cal.get(Calendar.MONTH), note.cal.get(Calendar.DATE), note.cal.get(Calendar.HOUR_OF_DAY), note.cal.get(Calendar.MINUTE));
		copyddmmyyyy = ((note.cal.get(Calendar.DATE)<10) ? "0" + Integer.toString(note.cal.get(Calendar.DATE)) : Integer.toString(note.cal.get(Calendar.DATE))) + "." + (((note.cal.get(Calendar.MONTH)<9) ? "0" + Integer.toString(note.cal.get(Calendar.MONTH)+1) : Integer.toString(note.cal.get(Calendar.MONTH)+1)) + "." + Integer.toString(note.cal.get(Calendar.YEAR)));
		copyhhmm= (((note.cal.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(note.cal.get(Calendar.HOUR_OF_DAY)) : Integer.toString(note.cal.get(Calendar.HOUR_OF_DAY)))+ ":" + ((note.cal.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(note.cal.get(Calendar.MINUTE)) : Integer.toString(note.cal.get(Calendar.MINUTE))));
	}
	
	public void notchanged(){
		presentnote.cal.set(copycal.get(Calendar.YEAR), copycal.get(Calendar.MONTH), copycal.get(Calendar.DATE), copycal.get(Calendar.HOUR_OF_DAY), copycal.get(Calendar.MINUTE));
		presentnote.ddmmyyyy.setText(copyddmmyyyy);
		presentnote.hhmm.setText(copyhhmm);
	}
	
	public void set(Note note){
		presentnote= note;
		owncalendar = note.cal;
		ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + (((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR))));
		hhmm.setText(((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY))) + ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));
		text.setText(note.text.getText());
	}
	
	public Note save(){
		presentnote.ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + (((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR))));
		presentnote.hhmm.setText((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) + ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));
		presentnote.text.setText(text.getText());
		return presentnote;
	}
	
	private void Init(){
		setSize(360,320);
		setLocation(0,0);	
		setLayout(null);
		font = new Font("TimesRoman", Font.PLAIN, 18);
		save = new JButton("Zapisz");
		cancel = new JButton("Anuluj");
		ddmmyyyy= new JLabel();
		hhmm = new JLabel();
		text= new JTextArea();
		nextd = new JButton(new ImageIcon(getClass().getResource("nextdmy.png")));
		prevd = new JButton(new ImageIcon(getClass().getResource("prevdmy.png")));
		nextm = new JButton(new ImageIcon(getClass().getResource("nextdmy.png")));
		prevm = new JButton(new ImageIcon(getClass().getResource("prevdmy.png")));
		nexty = new JButton(new ImageIcon(getClass().getResource("nextdmy.png")));
		prevy = new JButton(new ImageIcon(getClass().getResource("prevdmy.png")));
		nexth = new JButton(new ImageIcon(getClass().getResource("nextdmy.png")));
		prevh = new JButton(new ImageIcon(getClass().getResource("prevdmy.png")));
		nextmin = new JButton(new ImageIcon(getClass().getResource("nextdmy.png")));
		prevmin = new JButton(new ImageIcon(getClass().getResource("prevdmy.png")));
		save.setBounds(70,245,100,20);
		cancel.setBounds(190,245,100,20);
		ddmmyyyy.setBounds(90,23,100,20);
		ddmmyyyy.setFont(font);
		hhmm.setBounds(220,23,60,20);
		hhmm.setFont(font);
		nextd.setBounds(90,3,20,20);
		prevd.setBounds(90,43,20,20);
		nextm.setBounds(116,3,20,20);
		prevm.setBounds(116,43,20,20);
		nexty.setBounds(150,3,20,20);
		prevh.setBounds(220,43,20,20);
		nexth.setBounds(220,3,20,20);
		prevmin.setBounds(245,43,20,20);
		nextmin.setBounds(245,3,20,20);
		prevy.setBounds(150,43,20,20);
		JScrollPane scrollPane = new JScrollPane(text);
		scrollPane.setBounds(37,70,280,160);
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		add(ddmmyyyy);
		add(hhmm);
		add(scrollPane);
		add(save);
		add(cancel);
		add(prevd);
		add(nextd);
		add(prevm);
		add(nextm);
		add(prevy);
		add(nexty);
		add(prevh);
		add(nexth);
		add(prevmin);
		add(nextmin);
		nextd.addActionListener(this);
		prevd.addActionListener(this);
		nextm.addActionListener(this);
		prevm.addActionListener(this);
		nexty.addActionListener(this);
		prevy.addActionListener(this);
		nexth.addActionListener(this);
		prevh.addActionListener(this);
		nextmin.addActionListener(this);
		prevmin.addActionListener(this);
		copycal = Calendar.getInstance();
	}
	
	EditNotePanel() {
		Init();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source==prevd){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE)-1);
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
			}
		if(source==nextd){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE)+1);
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
		}		
		if(source==prevm){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH)-1, owncalendar.get(Calendar.DATE));
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
		}
		if(source==nextm){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH)+1, owncalendar.get(Calendar.DATE));
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
		}
		if(source==prevy){
			owncalendar.set(owncalendar.get(Calendar.YEAR)-1, owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE));
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
		}
		if(source==nexty){
			owncalendar.set(owncalendar.get(Calendar.YEAR)+1, owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE));
			ddmmyyyy.setText(((owncalendar.get(Calendar.DATE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.DATE)) : Integer.toString(owncalendar.get(Calendar.DATE))) + "." + ((owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(owncalendar.get(Calendar.MONTH)+1) : Integer.toString(owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(owncalendar.get(Calendar.YEAR)));
		}

		if(source==prevh){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE), owncalendar.get(Calendar.HOUR_OF_DAY)-1, owncalendar.get(Calendar.MINUTE));
			hhmm.setText(((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));
		}
		if(source==nexth){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE), owncalendar.get(Calendar.HOUR_OF_DAY)+1, owncalendar.get(Calendar.MINUTE));
			hhmm.setText(((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));

		}
		if(source==prevmin){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE), owncalendar.get(Calendar.HOUR_OF_DAY), owncalendar.get(Calendar.MINUTE)-1);
			hhmm.setText(((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));

		}
		if(source==nextmin){
			owncalendar.set(owncalendar.get(Calendar.YEAR), owncalendar.get(Calendar.MONTH), owncalendar.get(Calendar.DATE), owncalendar.get(Calendar.HOUR_OF_DAY), owncalendar.get(Calendar.MINUTE)+1);
			hhmm.setText(((owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(owncalendar.get(Calendar.MINUTE)) : Integer.toString(owncalendar.get(Calendar.MINUTE))));

		}
	}

}
