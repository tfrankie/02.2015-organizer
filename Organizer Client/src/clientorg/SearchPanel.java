package clientorg;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;


public class SearchPanel extends JPanel implements MouseListener{
	public JButton search;
	public JButton cancel;
	public JTextField text;
	public JScrollPane scrollpane;
	public JPanel viewpanel;
	private Dimension size;
	private int clickcount;
	
	public void refresh(ArrayList<Note> list, String key){
		clickcount=0;
		int notescount = 0;
		viewpanel.removeAll();
		int posx=3;
		int posy=-20;
		int num = 0;
	      while (list.size() > num) {
	    	  if(list.get(num).text.getText().toLowerCase().contains(key.toLowerCase())){
		    	  posy+=20;
		    	  list.get(num).setLocation(posx,posy);
		    	  this.viewpanel.add(list.get(num));
		    	  notescount++;
	    	  }
	    	  num++;
	      }
	      size.setSize(310,notescount*20);
	      viewpanel.setPreferredSize(size);
	      viewpanel.repaint();
	}
	
	private void Init(){
		setSize(360,320);
		setLocation(0,0);	
		setLayout(null);
		search = new JButton("Szukaj");
		cancel = new JButton("Anuluj");
		text = new JTextField("Wpisz wyszukiwaną fraze");
		viewpanel = new JPanel();
		viewpanel.setLayout(null);
		size = new Dimension(310,20);
		viewpanel.setPreferredSize(size);
		scrollpane= new JScrollPane(viewpanel);
		scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		text.setBounds(80,20,200,20);
		search.setBounds(60,50,100,20);
		cancel.setBounds(195,50,100,20);
		scrollpane.setBounds(12, 80, 330, 200);
		add(text);
		add(search);
		add(cancel);
		add(scrollpane);
	}
	
	SearchPanel(){
		Init();
		text.addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(clickcount==0) text.setText("");
		clickcount++;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
			
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
