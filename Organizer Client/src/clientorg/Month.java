package clientorg;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.LinkedList;

public class Month extends JPanel implements ActionListener{
	private int firstdayofmonth;
	private int ndays;
	static int DaysOnMonth[] = {31,28,31,30,31,30,31,31,30,31,30,31};
	static String month[] = {"STY","LUT","MAR","KWI","MAJ","CZE","LIP","SIE","WRZ","PAZ","LIS","GRU"};
	private Calendar help;
	private Calendar calendar;
	public DayButton daybutton[];
	private JLabel monthyear;

	
	private void Init(){
		setSize(360,320);
		setLocation(3,25);
		setLayout(null);
		JButton tyg = new JButton("pn            wt            �r            cz            pt            so           nd");
		tyg.setOpaque(false);
		tyg.setContentAreaFilled(false);
		tyg.setBorderPainted(false);
		tyg.setBounds(0,20,350,15);
		add(tyg);
		help = Calendar.getInstance();
	}
	
	public void refresh(ArrayList<Note> notelist){
		for (int i=0;i<ndays;i++){
			daybutton[i].resetNotesCount();
			int num = 0;
		    while (notelist.size() > num) {
		    	  Note note = notelist.get(num);
		    	  if((i+1 == note.cal.get(Calendar.DATE)) && (calendar.get(Calendar.MONTH) == note.cal.get(Calendar.MONTH)) && (calendar.get(Calendar.YEAR) == note.cal.get(Calendar.YEAR)))
		  	    	daybutton[i].addNotesCount();
		    	  num++;
		    }
		}
	}
	
	Month(Calendar calendar){
		Init();
		this.calendar = Calendar.getInstance();
		this.calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE));
		monthyear = new JLabel(month[(calendar.get(Calendar.MONTH))]+" " + Integer.toString(calendar.get(Calendar.YEAR)));
		monthyear.setBounds(160, 0, 100, 20);
		add(monthyear);
		help.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
		firstdayofmonth=help.get(Calendar.DAY_OF_WEEK);
		ndays=DaysOnMonth[calendar.get(Calendar.MONTH)];
		daybutton=new DayButton[ndays];
		int posx=0;
		int posy=0;
		if(firstdayofmonth==1)
			firstdayofmonth=7;
		else
			firstdayofmonth-=1;
		for (int i=0;i<ndays;i++)
		{
			if(i==0)
			{
				posx=50+(50*(firstdayofmonth-2));
				posy+=35;
			}
			else
			{
				if(firstdayofmonth==1 && i%7==0){
					posx=0;
					posy+=38;
				}
				else if(i%7==(7-(firstdayofmonth-1)))
				{
					posx=0;
					posy+=38;
				}
				else
				{
					posx+=50;
				}
			}
			help = Calendar.getInstance();
			daybutton[i]= new DayButton(i+1,posx,posy,(i==(help.get(Calendar.DAY_OF_MONTH)-1)&&(calendar.get(Calendar.MONTH)==help.get(Calendar.MONTH))&&(calendar.get(Calendar.YEAR)==help.get(Calendar.YEAR))));
			add(daybutton[i]);
			daybutton[i].addActionListener(this);
			daybutton[i].setActionCommand(Integer.toString(i+1));
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
	    MainPanel parent=(MainPanel) getParent();
	    help.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), Integer.valueOf(e.getActionCommand()));
    	parent.otherday.setDate(help);
    	parent.otherday.refresh(parent.notelist);
    	parent.add(parent.otherday);
    	parent.month.setVisible(false);
    	parent.othermonth.setVisible(false);
    	parent.year.setVisible(false);
    	parent.otherday.setVisible(true);
		parent.Rok.setEnabled(true);
		parent.Miesiac.setEnabled(true);
		parent.Dzien.setEnabled(false);
	}
}
