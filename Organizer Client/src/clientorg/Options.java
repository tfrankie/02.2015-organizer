package clientorg;

import javax.swing.JPanel;
import javax.swing.JButton;

public class Options extends JPanel{
	public JButton notatki;
	public JButton dodaj;
	public JButton szukaj;
	public JButton importuj;
	public JButton exportuj;
	public JButton close;
	
	Options(){
		setSize(100,150);
		setLocation(0,25);	
		setLayout(null);
		notatki = new JButton("NOTATKI");
		dodaj = new JButton("DODAJ");
		szukaj = new JButton("SZUKAJ");
		importuj = new JButton("IMPORTUJ");
		exportuj = new JButton("EXPORTUJ");
		close = new JButton("ZAMKNIJ");
		notatki.setBounds(0,0,100,25);
		dodaj.setBounds(0,25,100,25);
		szukaj.setBounds(0,50,100,25);
		importuj.setBounds(0,75,100,25);
		exportuj.setBounds(0,100,100,25);
		close.setBounds(0,125,100,25);
		add(notatki);
		add(dodaj);
		add(szukaj);
		add(importuj);
		add(exportuj);
		add(close);
		
	}
}
