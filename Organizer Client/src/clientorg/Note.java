package clientorg;

import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Note extends JPanel{
	
	public JLabel ddmmyyyy, hhmm, text;
	public JButton edit, delete;
	public Calendar cal;
	public int index;
	public int id;
	
	Note(int id, Calendar date, String t){
		setSize(310,20);
		setLayout(null);
		this.id = id;
		cal = Calendar.getInstance();
		cal.set(date.get(Calendar.YEAR),date.get(Calendar.MONTH),date.get(Calendar.DATE),date.get(Calendar.HOUR_OF_DAY),date.get(Calendar.MINUTE));
		ddmmyyyy=new JLabel(((date.get(Calendar.DATE)<10) ? "0" + Integer.toString(date.get(Calendar.DATE)) : Integer.toString(date.get(Calendar.DATE))) + "." + ((date.get(Calendar.MONTH)<9) ? "0" + Integer.toString(date.get(Calendar.MONTH)+1) : Integer.toString(date.get(Calendar.MONTH)+1)) + "." + Integer.toString(date.get(Calendar.YEAR)));	
		text = new JLabel(t);
		hhmm = new JLabel(((date.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(date.get(Calendar.HOUR_OF_DAY)) : Integer.toString(date.get(Calendar.HOUR_OF_DAY)))+ ":" + ((date.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(date.get(Calendar.MINUTE)) : Integer.toString(date.get(Calendar.MINUTE))));
		edit = new JButton(new ImageIcon(getClass().getResource("edit.png")));
		delete = new JButton(new ImageIcon(getClass().getResource("delete.png")));
		ddmmyyyy.setBounds(0,0,70,20);
		hhmm.setBounds(70,0,40,20);
		text.setBounds(110,0,155,20);
		edit.setBounds(270,0,20,20);
		delete.setBounds(290,0,20,20);
		add(edit);
		add(delete);
		add(ddmmyyyy);
		add(text);
		add(hhmm);
	}
	
	public void setIndex(int i){
		index=i;
	}
}
