package clientorg;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class AllNotes extends JPanel{

	private JLabel allnotes;
	public JButton cancel;
	public JScrollPane scrollpane;
	public JPanel viewpanel;
	private ArrayList<Note> notelist;
	private Dimension size;
	
	public void refresh(ArrayList<Note> list){
		int notescount = 0;
		notelist=list;
		this.viewpanel.removeAll();
		int posx=3;
		int posy=-20;
		int num = 0;
	      while (notelist.size() > num) {
	    	  posy+=20;
	    	  notelist.get(num).setLocation(posx,posy);
	    	  this.viewpanel.add(notelist.get(num));
	    	  num++;
		      notescount++;
	      }
	      size.setSize(310,notescount*20);
	      viewpanel.setPreferredSize(size);
	      this.viewpanel.repaint();
	}
	
	AllNotes(){
		setSize(360,320);
		setLocation(0,0);	
		setLayout(null);
		size = new Dimension(310,20);
		viewpanel = new JPanel();
		viewpanel.setPreferredSize(size);
		viewpanel.setLayout(null);
		scrollpane= new JScrollPane(viewpanel);
		scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollpane.setBounds(12, 30, 330, 220);
		cancel = new JButton("Wyj�cie");
		allnotes= new JLabel("Wszystkie notatki");
		allnotes.setBounds(120,0,150,20);
		cancel.setBounds(140,260,80,20);
		add(allnotes);
		add(cancel);
		add(scrollpane);
	}
}
