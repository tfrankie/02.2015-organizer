package clientorg;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import ServerIntf.ServIntf;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;


public class ClientFrame extends JFrame implements ActionListener, MouseListener, WindowListener{
	public Options options;
	public Calendar calendar;
	public MainPanel mainPanel;
	public AddNotePanel addnotepanel;
	public ArrayList<Note> notelist;
	public AllNotes allnotes;
	public JFileChooser fc;
	public EditNotePanel editnotepanel;
	private static int noteindex = -1;
	private boolean optionsfocused;
	public SearchPanel searchpanel;
	private String searchkey;
	public LoginPanel loginpanel;
	public boolean connected;
	public ServIntf server;
	private Registry registry;
	public String login, password;
	public Reminder reminder;
	
	private void Init(){
		calendar = Calendar.getInstance();
		if((calendar.get(Calendar.MONTH)+1)<10)
			setTitle(calendar.get(Calendar.DAY_OF_MONTH)+".0"+ (calendar.get(Calendar.MONTH)+1)+"."+ calendar.get(Calendar.YEAR));
		else
			setTitle(calendar.get(Calendar.DAY_OF_MONTH)+"."+ (calendar.get(Calendar.MONTH)+1)+"."+ calendar.get(Calendar.YEAR));
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(360,320);
		setResizable(false);
		setLocation(300,150);
		setVisible(true);
		loginpanel = new LoginPanel();
		addnotepanel= new AddNotePanel();
		options = new Options();
		notelist = new ArrayList<Note>();
		allnotes = new AllNotes();
		allnotes.setVisible(false);
		fc = new JFileChooser();
	    FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT Files", "txt");
	    fc.setFileFilter(filter);
	    editnotepanel = new EditNotePanel();
	    editnotepanel.setVisible(false);
	    searchpanel = new SearchPanel();
	    searchpanel.setVisible(false);
	    options.setVisible(false);
  		mainPanel = new MainPanel(calendar,notelist);
		mainPanel.day.addnote.addActionListener(this);
		mainPanel.otherday.addnote.addActionListener(this);		
		mainPanel.Menu.addActionListener(this);	
		addWindowListener(this);
		connected= false;
		
		reminder = new Reminder(notelist,this);
		
	  //POCZATKOWY WIDOK
    	add(loginpanel);
    	setContentPane(loginpanel);
    	loginpanel.cancel.addActionListener(this);
    	loginpanel.connect.addActionListener(this);
	    	
   //DODANIE S�UCHACZY	
  		options.close.addActionListener(this);
  		options.dodaj.addActionListener(this);		
  		options.importuj.addActionListener(this);		
  		options.exportuj.addActionListener(this);
  		options.notatki.addActionListener(this);
  		options.szukaj.addActionListener(this);
  		addnotepanel.cancel.addActionListener(this);
  		addnotepanel.add.addActionListener(this);
  		allnotes.cancel.addActionListener(this);
  		editnotepanel.cancel.addActionListener(this);
  		editnotepanel.save.addActionListener(this);
  		this.addMouseListener(this);
  		searchpanel.cancel.addActionListener(this);
  		searchpanel.search.addActionListener(this);  	
	}
	
	ClientFrame(){
		Init();
	}
	
	private void refreshview(){
		if(allnotes.isVisible()){
	      	allnotes.refresh(notelist);	
	      }
	      else{
	    	  if(searchpanel.isVisible()){
	    		 searchpanel.refresh(notelist,searchkey);
	    	  }
	    	  else{
				if(!mainPanel.Miesiac.isEnabled()  && !mainPanel.othermonth.isVisible())
		      		mainPanel.month.refresh(notelist);	
			    else if(!mainPanel.Dzien.isEnabled() && !mainPanel.otherday.isVisible())
			      	mainPanel.day.refresh(notelist);
			    else if(mainPanel.otherday.isVisible())
			    	mainPanel.otherday.refresh(notelist);
			    else if(mainPanel.othermonth.isVisible())
			    	mainPanel.othermonth.refresh(notelist);	
	    	  }
	      }
	}

	public void logout(){
		reminder.setConnected(connected);
		connected = false;
		remove(mainPanel);
  		setContentPane(loginpanel);	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source==mainPanel.Menu)
		{
			options.setVisible(true);
			add(options);
			setContentPane(options);
		}
		if(source==options.close){
			try {
				server.logout(login);
				server.refreshUser(login);
		} catch (RemoteException e1) {
			System.out.println("B��d z wylogowaniem.");
		}
		connected = false;
		System.exit(0);
		}
		if(source==options.dodaj){
			addnotepanel.setDate(calendar);
			add(addnotepanel);
			setContentPane(addnotepanel);
		}
		if(source==addnotepanel.cancel){
			remove(addnotepanel);
			setContentPane(mainPanel);
			addnotepanel.text.setText("");
			addnotepanel.owncalendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE),calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE));
			addnotepanel.ddmmyyyy.setText(Integer.toString(addnotepanel.owncalendar.get(Calendar.DATE)) + "." + (((addnotepanel.owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(addnotepanel.owncalendar.get(Calendar.MONTH)+1) : Integer.toString(addnotepanel.owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(addnotepanel.owncalendar.get(Calendar.YEAR))));
			addnotepanel.hhmm.setText(((addnotepanel.owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(addnotepanel.owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(addnotepanel.owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((addnotepanel.owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(addnotepanel.owncalendar.get(Calendar.MINUTE)) : Integer.toString(addnotepanel.owncalendar.get(Calendar.MINUTE))));	
		}

		if(source==addnotepanel.add){
			try {
				int lastid = server.lastid();
				lastid++;
				Note note= new Note(lastid,addnotepanel.owncalendar, addnotepanel.text.getText());
				String notatka = Integer.toString(lastid) + "." + note.cal.get(Calendar.DATE) + "." + note.cal.get(Calendar.MONTH) + "." + note.cal.get(Calendar.YEAR) + "." + note.cal.get(Calendar.HOUR_OF_DAY) + "." + note.cal.get(Calendar.MINUTE) + "." + note.text.getText();
				noteindex++;
				note.setIndex(noteindex);
				note.edit.addActionListener(this);
				note.delete.addActionListener(this);
				note.edit.setActionCommand("noteedit"+ Integer.toString(noteindex));
				note.delete.setActionCommand("notedelete"+ Integer.toString(noteindex));
				notelist.add(note);
				remove(addnotepanel);	      	
				if(!mainPanel.Miesiac.isEnabled()  && !mainPanel.othermonth.isVisible())
		      		mainPanel.month.refresh(notelist);	
			    else if(!mainPanel.Dzien.isEnabled() && !mainPanel.otherday.isVisible())
			      	mainPanel.day.refresh(notelist);
			    else if(mainPanel.otherday.isVisible())
			    	mainPanel.otherday.refresh(notelist);
			    else if(mainPanel.othermonth.isVisible())
			    	mainPanel.othermonth.refresh(notelist);
				setContentPane(mainPanel);
				addnotepanel.text.setText("");
				addnotepanel.owncalendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE),calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE));
				addnotepanel.ddmmyyyy.setText(Integer.toString(addnotepanel.owncalendar.get(Calendar.DATE)) + "." + (((addnotepanel.owncalendar.get(Calendar.MONTH)<9) ? "0" + Integer.toString(addnotepanel.owncalendar.get(Calendar.MONTH)+1) : Integer.toString(addnotepanel.owncalendar.get(Calendar.MONTH)+1)) + "." + Integer.toString(addnotepanel.owncalendar.get(Calendar.YEAR))));
				addnotepanel.hhmm.setText(((addnotepanel.owncalendar.get(Calendar.HOUR_OF_DAY)<10) ? "0" + Integer.toString(addnotepanel.owncalendar.get(Calendar.HOUR_OF_DAY)) : Integer.toString(addnotepanel.owncalendar.get(Calendar.HOUR_OF_DAY)))+ ":" + ((addnotepanel.owncalendar.get(Calendar.MINUTE)<10) ? "0" + Integer.toString(addnotepanel.owncalendar.get(Calendar.MINUTE)) : Integer.toString(addnotepanel.owncalendar.get(Calendar.MINUTE))));
				server.addNote(login,lastid,notatka);
			} catch (RemoteException e2) {
				System.out.println("Remote exception");
			}
			
		}
		if(source==options.notatki){
			allnotes.setVisible(true);
			allnotes.refresh(notelist);
			add(allnotes);
			setContentPane(allnotes);
		}
		if(source==allnotes.cancel){
			remove(allnotes);
			allnotes.setVisible(false);
			setContentPane(mainPanel);
			if(!mainPanel.Miesiac.isEnabled()  && !mainPanel.othermonth.isVisible())
	      		mainPanel.month.refresh(notelist);	
		    else if(!mainPanel.Dzien.isEnabled() && !mainPanel.otherday.isVisible())
		      	mainPanel.day.refresh(notelist);
		    else if(mainPanel.otherday.isVisible())
		    	mainPanel.otherday.refresh(notelist);
		    else if(mainPanel.othermonth.isVisible())
		    	mainPanel.othermonth.refresh(notelist);
		}
		
		if(source==options.importuj){
			importuj();
		}
		if(source==options.exportuj){
			exportuj();
		}
		if(source==options.szukaj){
			searchpanel.setVisible(true);
			add(searchpanel);
			if(!mainPanel.Miesiac.isEnabled()  && !mainPanel.othermonth.isVisible())
	      		mainPanel.month.refresh(notelist);	
		    else if(!mainPanel.Dzien.isEnabled() && !mainPanel.otherday.isVisible())
		      	mainPanel.day.refresh(notelist);
		    else if(mainPanel.otherday.isVisible())
		    	mainPanel.otherday.refresh(notelist);
		    else if(mainPanel.othermonth.isVisible())
		    	mainPanel.othermonth.refresh(notelist);
			setContentPane(searchpanel);
		}		
		if(source==searchpanel.cancel){
			remove(searchpanel);
			if(!mainPanel.Miesiac.isEnabled()  && !mainPanel.othermonth.isVisible())
	      		mainPanel.month.refresh(notelist);	
		    else if(!mainPanel.Dzien.isEnabled() && !mainPanel.otherday.isVisible())
		      	mainPanel.day.refresh(notelist);
		    else if(mainPanel.otherday.isVisible())
		    	mainPanel.otherday.refresh(notelist);
		    else if(mainPanel.othermonth.isVisible())
		    	mainPanel.othermonth.refresh(notelist);
			setContentPane(mainPanel);
			searchpanel.setVisible(false);
			searchpanel.text.setText("Wpisz wyszukiwan� fraze");
		}		
		if(source==searchpanel.search){
			searchkey = searchpanel.text.getText();
			searchpanel.refresh(notelist, searchkey);
		}
		
		if(e.getActionCommand().startsWith("note")){
			String command = e.getActionCommand();
			
			if(command.startsWith("noteedit")){
				searchpanel.setVisible(false);
				editnotepanel.setVisible(true);
				int lastletter = Integer.parseInt(command.substring(8));
				int num = 0;
			      while (notelist.size() > num) {
			    	  if(notelist.get(num).index == lastletter){
			    		  	editnotepanel.keepnote(notelist.get(num));
							editnotepanel.set(notelist.get(num));
							add(editnotepanel);
							setContentPane(editnotepanel);
			    	  }
			    	  num++;
			      } 
			}
	
			if(command.startsWith("notedelete")){
				int lastletter = Integer.parseInt(command.substring(10));
				int num = 0;
			      while (notelist.size() > num) {
			    	  if(notelist.get(num).index == lastletter){
			    		  try {
								server.deleteNote(notelist.get(num).id,login);
							} catch (RemoteException e1) {
								System.out.println("RemoteExcep przy deletenote()");
							}
			    		  notelist.remove(notelist.get(num)); 
			    	  }
			    	  num++;
			      }  
			      refreshview();
			}
		}
		
		if(source == editnotepanel.cancel){
			editnotepanel.setVisible(false);
			editnotepanel.notchanged();
			remove(editnotepanel);
			setContentPane(mainPanel);
			if(!mainPanel.Miesiac.isEnabled()  && !mainPanel.othermonth.isVisible())
	      		mainPanel.month.refresh(notelist);	
		    else if(!mainPanel.Dzien.isEnabled() && !mainPanel.otherday.isVisible())
		      	mainPanel.day.refresh(notelist);
		    else if(mainPanel.otherday.isVisible())
		    	mainPanel.otherday.refresh(notelist);
		    else if(mainPanel.othermonth.isVisible())
		    	mainPanel.othermonth.refresh(notelist);
		}

		if(source == editnotepanel.save){
			editnotepanel.setVisible(false);
			Note notatka = editnotepanel.save();
			remove(editnotepanel);
			setContentPane(mainPanel);
			if(!mainPanel.Miesiac.isEnabled()  && !mainPanel.othermonth.isVisible())
	      		mainPanel.month.refresh(notelist);	
		    else if(!mainPanel.Dzien.isEnabled() && !mainPanel.otherday.isVisible())
		      	mainPanel.day.refresh(notelist);
		    else if(mainPanel.otherday.isVisible())
		    	mainPanel.otherday.refresh(notelist);
		    else if(mainPanel.othermonth.isVisible())
		    	mainPanel.othermonth.refresh(notelist);
			try {
				String presentnote = notatka.id + "." + notatka.cal.get(Calendar.DATE) + "." + notatka.cal.get(Calendar.MONTH) + "." + notatka.cal.get(Calendar.YEAR) + "." + notatka.cal.get(Calendar.HOUR_OF_DAY) + "." + notatka.cal.get(Calendar.MINUTE) + "." + notatka.text.getText();
				server.editNote(login,presentnote);
			} catch (RemoteException e1) {
				System.out.println("RemotExcept przy editnote");
			}
		}
	if(source == mainPanel.day.addnote || source == mainPanel.otherday.addnote){
		if(mainPanel.otherday.isVisible())
			addnotepanel.setDate(mainPanel.otherday.getDate());
		else
			addnotepanel.setDate(mainPanel.day.getDate());
		add(addnotepanel);
		setContentPane(addnotepanel);
	}
	
	if(source == loginpanel.cancel){
		System.exit(0);
	}
	
	if(source == loginpanel.connect){
		polacz();
	}
	}
	
	private void exportuj(){
		if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
			String nameOfFile=fc.getSelectedFile().getAbsolutePath();
			if(!nameOfFile.toLowerCase().endsWith(".txt"))
			{
			     nameOfFile+=".txt";
			}
			File plik = new File(nameOfFile);
			try {
				PrintWriter out = new PrintWriter(plik);
				int num = 0;
			      while (notelist.size() > num) {
					out.println(Integer.toString(notelist.get(num).id) + "." + Integer.toString(notelist.get(num).cal.get(Calendar.DATE)) + "." + Integer.toString(notelist.get(num).cal.get(Calendar.MONTH)) + "." + Integer.toString(notelist.get(num).cal.get(Calendar.YEAR)) + "." + notelist.get(num).cal.get(Calendar.HOUR_OF_DAY) + "." + notelist.get(num).cal.get(Calendar.MINUTE));
					out.println(notelist.get(num).text.getText());
			    	num++;
			      }
				out.close();
			} catch (IOException e1) {
				System.out.println("B�ad IOException");
			}
			JOptionPane.showMessageDialog(this, "Pomy�lnie wyexportowano.", "Export" , JOptionPane.INFORMATION_MESSAGE);
			remove(options);
			setContentPane(mainPanel);
		}
	}
	private void importuj(){
		if(fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
			Calendar impcal = Calendar.getInstance();
			String text = new String();
			int id,year,month,date,hour,minute;
			File plik = fc.getSelectedFile();
			try {
				Scanner scanner = new Scanner(plik);
				while(scanner.hasNext()){
					StringTokenizer stringTokenizer = new StringTokenizer(scanner.nextLine(), ".");
					id = Integer.valueOf(stringTokenizer.nextToken());
					date= Integer.valueOf(stringTokenizer.nextToken());
					month=Integer.valueOf(stringTokenizer.nextToken());
					year=Integer.valueOf(stringTokenizer.nextToken());
					hour=Integer.valueOf(stringTokenizer.nextToken());
					minute=Integer.valueOf(stringTokenizer.nextToken());
					impcal.set(year,month,date,hour,minute);
					text=scanner.nextLine();
					Note note= new Note(id,impcal,text);
					notelist.add(note);
					note.edit.addActionListener(this);
					note.delete.addActionListener(this);
					noteindex++;
					note.setIndex(noteindex);
					note.edit.setActionCommand("noteedit"+ Integer.toString(noteindex));
					note.delete.setActionCommand("notedelete"+ Integer.toString(noteindex));
				}
				scanner.close();
				
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(this, "Pomy�lnie zaimportowano.", "Import" , JOptionPane.INFORMATION_MESSAGE);
			remove(options);
			refreshview();
			setContentPane(mainPanel);
		}
	}
	
	private void importfromserver() throws RemoteException{
		loginpanel.statusField.setText("Importowanie notatek z serwera.");
		notelist.clear();
		while(!server.canexport(login)){
			loginpanel.statusField.setText("Oczekuje na import z serwera.");
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				System.out.println("B��d sleep()");
			}
		}
		int howmanynotes = server.howmanynotes(login);
			Calendar impcal = Calendar.getInstance();
			int id,year,month,date,hour,minute;
			String text;
		for(int i=0; i<howmanynotes; i++)
		{
			String notatka = server.importNote(login);
			StringTokenizer stringTokenizer = new StringTokenizer(notatka, ".");
			id = Integer.valueOf(stringTokenizer.nextToken());
			date= Integer.valueOf(stringTokenizer.nextToken());
			month=Integer.valueOf(stringTokenizer.nextToken());
			year=Integer.valueOf(stringTokenizer.nextToken());
			hour=Integer.valueOf(stringTokenizer.nextToken());
			minute=Integer.valueOf(stringTokenizer.nextToken());
			text= (stringTokenizer.countTokens() > 0 ? stringTokenizer.nextToken() : "");
			impcal.set(year,month,date,hour,minute);
			Note note= new Note(id,impcal,text);
			notelist.add(note);
			note.edit.addActionListener(this);
			note.delete.addActionListener(this);
			noteindex++;
			note.setIndex(noteindex);
			note.edit.setActionCommand("noteedit"+ Integer.toString(noteindex));
			note.delete.setActionCommand("notedelete"+ Integer.toString(noteindex));
		}
		server.finishedimporting();
		reminder.setConnected(connected);
		refreshview();
	}
	
	private void polacz(){
		try {
			registry = LocateRegistry.getRegistry(loginpanel.ipField.getText(),8080);
			server = (ServIntf) registry.lookup("ServRMI");
			try {
				login = loginpanel.login.getText();
				password = loginpanel.password.getText();
				if(server.login(login,password)){
							try {
								if(server.isOpen())
								{
									if (!connected) {
										connected = true;
										add(mainPanel);
								  		setContentPane(mainPanel);
										importfromserver();	
									}   
								}
								else{
									loginpanel.statusField.setText("Serwer nieczynny.");
								}
							} catch (RemoteException e1) {
								loginpanel.statusField.setText("B��d servera");
							}
				}
				else{
					loginpanel.statusField.setText("Z�y login lub has�o.");
				}
			} catch (RemoteException e1) {
				loginpanel.statusField.setText("B��d serwera");
			}
		} catch (AccessException ex) {
			loginpanel.statusField.setText("Serwer nieczynny.");
		} catch (RemoteException ex) {
			loginpanel.statusField.setText("Serwer nieczynny.");
		} catch (NotBoundException ex) {
			loginpanel.statusField.setText("Serwer nieczynny.");
		}
		
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		if(options.isVisible() && !optionsfocused)
		{
			options.setVisible(false);
			remove(options);
			setContentPane(mainPanel);
		}
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		if(this.getContentPane()==options)
			optionsfocused = false;
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		if(this.getContentPane()==options)
			optionsfocused = true;
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
	}
	@Override
	
	public void mouseReleased(MouseEvent arg0) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		if(connected)
		try {
			server.logout(login);
			server.refreshUser(login);
		} catch (RemoteException e1) {
			System.out.println("B��d przy wylogowywaniu");
		}
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

}
