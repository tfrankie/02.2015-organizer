package clientorg;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JOptionPane;


public class Reminder implements Runnable{
	
	private ArrayList<Note> notelist;
	private Calendar currenttime;
	private volatile boolean connected;
	private ClientFrame parentframe;
	private Clip clip;
	private AudioInputStream audiostream;
	private URL soundURL;
	private Mixer mixer;
	private DataLine.Info dataInfo;
	
	Reminder(ArrayList<Note> notelist, ClientFrame clientframe){
		currenttime = Calendar.getInstance();
		this.notelist = notelist;
		parentframe = clientframe;
		(new Thread(this)).start();
	}
	
	public void setConnected(boolean con){
		connected = con;
	}

	private void ring(){
		Mixer.Info[] mixInfos = AudioSystem.getMixerInfo();
		mixer = AudioSystem.getMixer(mixInfos[0]);
		dataInfo = new DataLine.Info(Clip.class, null);
		try {
			clip = (Clip)mixer.getLine(dataInfo);
			soundURL = Reminder.class.getResource("ring.wav");
			audiostream = AudioSystem.getAudioInputStream(soundURL);
			clip.open(audiostream);
		} catch (UnsupportedAudioFileException | IOException e) {
			System.out.println("BLAD AUDIO");
		} catch (LineUnavailableException e) {
			System.out.println("BLAD CLIP");
		}
		clip.start();
	}
	
	@Override
	public void run() {
		while(true){
			if(connected){
				currenttime = Calendar.getInstance();
				int num = 0;
			    while (notelist.size() > num) {
					if((notelist.get(num).cal.get(Calendar.DATE)) == currenttime.get(Calendar.DATE)
						&& (notelist.get(num).cal.get(Calendar.MONTH)) == currenttime.get(Calendar.MONTH)
						&& (notelist.get(num).cal.get(Calendar.YEAR)) == currenttime.get(Calendar.YEAR)
						&& (notelist.get(num).cal.get(Calendar.HOUR_OF_DAY)) == currenttime.get(Calendar.HOUR_OF_DAY)
						&& (notelist.get(num).cal.get(Calendar.MINUTE)) == currenttime.get(Calendar.MINUTE))
					{
						ring();
						JOptionPane.showMessageDialog(parentframe, notelist.get(num).text.getText(), "Notatka" , JOptionPane.INFORMATION_MESSAGE, null);
					}
					num++;
			    }
			}
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				System.out.println("b�ad sleep()");
			}
		}
	}

}
