package clientorg;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ContainerListener;

public class Year extends JPanel implements ActionListener{
	static String month[] = {"STY","LUT","MAR","KWI","MAJ","CZE","LIP","SIE","WRZ","PAZ","LIS","GRU"};
	public MonthButton monthbutton[] = new MonthButton[12];
	private Calendar newDate;
	private Calendar calendar;
	private JLabel year;
	
	
	public Year(Calendar calendar){
		this.calendar=calendar;
		newDate=Calendar.getInstance();
		setSize(360,320);
		setLocation(3,26);	
		setLayout(null);
		year=new JLabel(Integer.toString(calendar.get(Calendar.YEAR)));
		year.setBounds(160, 0, 100, 20);
		add(year);
		int posx=0;
		int posy=0;
		for(int i=0;i<12;i++)
		{
			{
				if(i%4==0)
				{
					if(i==0)
					{
						posx=0;
						posy=20;
					}
					else
					{
						posx=0;
						posy+=81;
					}
				}
				else
				{
					posx+=87;
				}
				monthbutton[i]= new MonthButton(month[i],posx,posy,((i==calendar.get(Calendar.MONTH)&&(calendar.get(Calendar.YEAR))==newDate.get(Calendar.YEAR))));
				add(monthbutton[i]);
				monthbutton[i].addActionListener(this);
				monthbutton[i].setActionCommand(Integer.toString(i));
			}
		}
	}


	@Override
	public void actionPerformed(ActionEvent e) {
	    MainPanel parent=(MainPanel) getParent();
	    	newDate.set(calendar.get(Calendar.YEAR), Integer.valueOf(e.getActionCommand()), 1);
	    	parent.othermonth=new Month(newDate);
	    	parent.othermonth.refresh(parent.notelist);
	    	parent.add(parent.othermonth);
	    	parent.year.setVisible(false);
	    	parent.otheryear.setVisible(false);
	    	parent.othermonth.setVisible(true);
			parent.Rok.setEnabled(true);
			parent.Miesiac.setEnabled(false);
			parent.Dzien.setEnabled(true);
	}
}
