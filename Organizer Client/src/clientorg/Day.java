package clientorg;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;

public class Day extends JPanel{
	
	private JButton text;
	public JScrollPane scrollpane;
	public JPanel viewpanel;
	public Calendar owncal;
	private ArrayList<Note> notelist;
	private Dimension size;
	public JButton addnote;
	
	public void refresh(ArrayList<Note> list){
		int notescount = 0;
		notelist=list;
		viewpanel.removeAll(); 
		int posx=3;
		int posy=-20;
		int num = 0;
	      while (notelist.size() > num) {
	    	  Note note= notelist.get(num);
	    	  if((note.cal.get(Calendar.DATE) == owncal.get(Calendar.DATE)) && (note.cal.get(Calendar.MONTH) == owncal.get(Calendar.MONTH)) && (note.cal.get(Calendar.YEAR) == owncal.get(Calendar.YEAR)))
	    	  {
		    	  posy+=20;
		    	  note.setLocation(posx,posy);
		    	  viewpanel.add(note);
			      notescount++;
	    	  }
	    	  num++;
	      }
	      addnote.setLocation(1,posy+20);
	      viewpanel.add(addnote);
	      size.setSize(310,(notescount+1)*20);
	      viewpanel.setPreferredSize(size);
	      viewpanel.repaint();
	}
	
	public void setDate(Calendar calendar){
		owncal.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
		if((calendar.get(Calendar.MONTH)+1)<10)
			text.setText(calendar.get(Calendar.DATE)+".0"+ (calendar.get(Calendar.MONTH)+1)+"."+ calendar.get(Calendar.YEAR));
		else
			text.setText(calendar.get(Calendar.DATE)+"."+ (calendar.get(Calendar.MONTH)+1)+"."+ calendar.get(Calendar.YEAR));
	}
	
	public Calendar getDate(){
		return owncal;
	}
	
	Day(Calendar calendar){
		setSize(360,320);
		setLocation(0,26);	
		setLayout(null);
		owncal = Calendar.getInstance();
		owncal.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
		text=new JButton();
		if((calendar.get(Calendar.MONTH)+1)<10)
			text.setText(calendar.get(Calendar.DATE)+".0"+ (calendar.get(Calendar.MONTH)+1)+"."+ calendar.get(Calendar.YEAR));
		else
			text.setText(calendar.get(Calendar.DATE)+"."+ (calendar.get(Calendar.MONTH)+1)+"."+ calendar.get(Calendar.YEAR));
		text.setBounds(130, 0, 100, 25);
		text.setOpaque(false);
		text.setContentAreaFilled(false);
		text.setBorderPainted(false);
		addnote = new JButton("Dodaj notatke");
		addnote.setSize(310, 20);
		viewpanel = new JPanel();
		viewpanel.setLayout(null);
		size = new Dimension(310,20);
		viewpanel.setPreferredSize(size);
		scrollpane= new JScrollPane(viewpanel);
		scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollpane.setBounds(12, 30, 330, 220);
		add(scrollpane);
		add(text);
	}
}
