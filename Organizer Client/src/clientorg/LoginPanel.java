package clientorg;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class LoginPanel extends JPanel{
	
	private JLabel loginlabel,passwordlabel;
	public JTextField login;
	public JPasswordField password;
	public JButton connect, cancel;
	private Font font;
	public JTextField ipField;
	public String hostIP; 
	public JLabel iplabel;
	public JLabel statusField;
	
	private void Init(){
		setSize(360,300);
		setLayout(null);
		setLocation(0,0);
		setVisible(true);
		statusField = new JLabel();
		statusField.setBounds(100,250,200,20);
		add(statusField);
		iplabel = new JLabel("IP: ");
		iplabel.setBounds(100,20,120,20);
		add(iplabel);
		hostIP = new String("127.0.0.1");
		ipField = new JTextField();
		ipField.setText(hostIP);
		ipField.setBounds(130,20,120,20);
		add(ipField);
		font = new Font("TimesRoman", Font.PLAIN, 20);
		loginlabel = new JLabel("Login:");
		loginlabel.setFont(font);
		loginlabel.setBounds(60,50,120,40);
		add(loginlabel);
		passwordlabel = new JLabel("Has�o:");
		passwordlabel.setFont(font);
		passwordlabel.setBounds(60,90,100,40);
		add(passwordlabel);
		connect = new JButton("Zaloguj");
		connect.setFont(font);
		connect.setBounds(110,140,150,40);
		add(connect);
		cancel = new JButton("Wyj�cie");
		cancel.setFont(font);
		cancel.setBounds(110,180,150,40);
		add(cancel);
		login = new JTextField();
		login.setFont(font);
		login.setBounds(130,50,150,40);
		add(login);
		password = new JPasswordField();
		password.setFont(font);
		password.setBounds(130,90,150,40);
		add(password);
	}
	
	LoginPanel(){
		Init();
	}

	public void refresh() throws IOException {
		login.setText("");
		password.setText("");
	}
}

