package clientorg;

import java.net.ConnectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public class MainClient extends UnicastRemoteObject{
	private ClientFrame clientframe;
	private static MainClient cli;

	protected MainClient() throws RemoteException {
		super();
		clientframe  = new ClientFrame();
	}
	
	public static void main(String[] args) throws RemoteException{
		cli = new MainClient();
		//NAS�UCHIWANIE SERWERA - WYLOGOWANIE GDY SERVER OFFLINE
		while(true){
			try {
				Thread.sleep(300);
				cli.keeplogin();
			} catch (InterruptedException | ConnectException e) {
				System.out.println("B�ad thread.sleep()");
			}
		}
	}
	
	private void keeplogin() throws ConnectException{
		try{
			if(clientframe.connected && !clientframe.server.checkOnline())
			{
				clientframe.connected = false;
				clientframe.remove(clientframe.mainPanel);
				clientframe.setContentPane(clientframe.loginpanel);
				clientframe.server.logout(clientframe.login);
				clientframe.server.refreshUser(clientframe.login);
				clientframe.reminder.setConnected(clientframe.connected);
				clientframe.loginpanel.statusField.setText("Roz��czono z serwerem.");
			}
		} catch(RemoteException e){
			clientframe.loginpanel.statusField.setText("B��d w po��czeniu z serwerem.");
			clientframe.connected = false;
			clientframe.remove(clientframe.mainPanel);
			clientframe.reminder.setConnected(clientframe.connected);
			clientframe.setContentPane(clientframe.loginpanel);
		}
	}
	
}
