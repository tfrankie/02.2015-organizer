package clientorg;

import javax.swing.JButton;

public class MonthButton extends JButton{
	static int height= 81;
	static int width= 87;
	private String name;
	MonthButton(String name, int x, int y, boolean present){
		this.name=name;
		setText(this.name);
		setBounds(x,y,width,height);
		setVisible(true);
		setContentAreaFilled(present);
	}
}