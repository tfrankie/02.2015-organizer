package serverorg;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class User {

	public String login;
	public int howmanynotes;
	public ArrayList<String> notes;
	
	User(String log, Connection con, PreparedStatement stmt){
		login=log;
		notes = new ArrayList<String>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM `Notes` WHERE `Owner` = ?");
			stmt.setString(1, login);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				String note = rs.getInt("ID") + ".";
				note += rs.getString("Day") + ".";
				note += rs.getString("Month") + ".";
				note += rs.getString("Year") + ".";
				note += rs.getString("Hour") + ".";
				note += rs.getString("Minute") + ".";
				note += rs.getString("Text");
				notes.add(note);
			}
		} catch (SQLException e) {
			System.out.println("sqlexception przy wczytywaniu uzytkownika");
		}
	}
	
	public void refresh(Connection con, PreparedStatement stmt){
		//WYCZYSZCZENIE LISTY I WCZYTANIE NA NOWO
		notes.clear();
		try {
			stmt = con.prepareStatement("SELECT * FROM `Notes` WHERE `Owner` = ?");
			stmt.setString(1, login);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				String note = rs.getInt("ID") + ".";
				note += rs.getString("Day") + ".";
				note += rs.getString("Month") + ".";
				note += rs.getString("Year") + ".";
				note += rs.getString("Hour") + ".";
				note += rs.getString("Minute") + ".";
				note += rs.getString("Text");
				notes.add(note);
			}
		} catch (SQLException e) {
			System.out.println("sqlexception przy wczytywaniu uzytkownika");
		}
	}
}
