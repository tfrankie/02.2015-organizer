package serverorg;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import ServerIntf.ServIntf;

public class MainServer extends UnicastRemoteObject implements ServIntf, ActionListener, WindowListener{
	private boolean opened;
	private ServerFrame serverframe;
	private static MainServer serv;
	private int usercount;
	public static String adres = "www.db4free.net";
    public static String nazwaBazy ="tfraorganizer" ;
    public static String nazwaUzyt = "tfrankie";
    public static String hasloBD = "org123";
    private Connection con;
    private PreparedStatement stmt;
    private boolean connectedtobd, connectedclient;
    private String importer = "empty";
    private int importingnum = 0;
    private static ArrayList<User> userslist;
	private static int lastid = 0;
   
	
	protected MainServer() throws RemoteException {
		super();
		opened = false;
		serverframe = new ServerFrame();
		serverframe.connectbutton.addActionListener(this);
		serverframe.addWindowListener(this);
	}
	
	public static void main(String[] args) {
		try {
			serv = new MainServer();
			Registry registry = LocateRegistry.createRegistry(8080);
			registry.bind("ServRMI", serv);
		} catch (RemoteException e1) {
			serv.serverframe.connectstatus.setText("B��d przy otwieraniu serwera.");
		} catch (AlreadyBoundException e1) {
			serv.serverframe.connectstatus.setText("B��d przy otwieraniu serwera.");
		}
		
		// LADOWANIE STEROWNIKA
		serv.serverframe.log.append("Sprawdzanie sterownika JDBC... \n");
        try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (Exception e) {
			System.out.println("Blad przy ladowaniu sterownika bazy!");
			System.exit(1);
		}
		serv.serverframe.log.append("Za�adowano sterownik JDBC.\n");
		
		//��CZENIE DO BAZY DANYCH
		serv.serverframe.log.append("��czenie do bazy danych...\n");
		try {
			serv.con = DriverManager.getConnection("jdbc:mysql://"+adres+"/"+nazwaBazy,nazwaUzyt,hasloBD);
			serv.connectedtobd = true;
			serv.serverframe.log.append("Po��czono z baz� danych.\n");
		} catch (SQLException e) {
			serv.serverframe.log.append("B��d w po��czeniu z baz� danych.\n");
			serv.connectedtobd = false;
		}
		
		//WCZYTANIE WSZYSTKICH UZYTKOWNIK�W.
		userslist=new ArrayList<User>();
		serv.serverframe.log.append("Wczytywanie zasob�w u�ytkownik�w...\n");
		try {
			serv.stmt = serv.con.prepareStatement("SELECT * FROM  `Users`");
			ResultSet rs = serv.stmt.executeQuery();
			while(rs.next()){
				userslist.add(new User(rs.getString("Login"), serv.con, serv.stmt));
			}
			serv.serverframe.log.append("Wczytano zasoby u�ytkownik�w.\n");
		} catch (SQLException e) {
			serv.serverframe.log.append("B��d przy wczytywaniu zasob�w u�ytkowni�w.\n");
		}
		
		
		//WYPISANIE LISTY UZYTKOWNIKOW ORAZ WCZYTANIE OSTANIEGO ID NOTATKI
		int num = 0;
		int id = 0;
	    while (userslist.size() > num) {
	    	  System.out.println("LOGIN: " + userslist.get(num).login);
	    	  int num1 = 0;
	    	  while(userslist.get(num).notes.size() > num1){
	    		  System.out.println(userslist.get(num).notes.get(num1));
	    		  
	    		  
	    		  //ZNALEZNIE LAST ID
	    		  StringTokenizer stringTokenizer = new StringTokenizer(userslist.get(num).notes.get(num1), ".");
	    		  id = Integer.valueOf(stringTokenizer.nextToken());
	    		  if(id > lastid) lastid = id;
	    		  
	    		  num1++;
	    	  }
	    	  num++;
	    }
	    
	    //GDY NIE MA NOTATEK RESETUJE AUTO_INCREMENT
	    if(lastid < 1 ) {
	    	try {
				String sql = "ALTER TABLE Notes AUTO_INCREMENT = 1";
				serv.stmt.executeUpdate(sql);
				serv.serverframe.log.append("Zresetowano auto_increment.\n");
			} catch (SQLException e) {
				serv.serverframe.log.append("B��d przy resetowaniu auto_increment.\n");
			}
	    }
		
	}
	
	public void openServer(){
		serverframe.log.append("Serwer: W��czenie serwera.\n");
		opened = true;
	}
	public void closeServer(){
		serverframe.log.append("Serwer: Wy��czenie serwera.\n");
		opened = false;
	}
	@Override
	public boolean isOpen() throws RemoteException {
		return opened;
	}
	@Override
	public boolean login(String l, String p) throws RemoteException{
		try {
			if(connectedtobd){
				connectedclient=true;
				stmt = con.prepareStatement("SELECT * FROM  `Users` WHERE  `Login` = ? AND  `Password` = ?");
				stmt.setString(1, l);
				stmt.setString(2, p);
				//JE�LI SERWER OPENED, i LOGIN, HASLO SIE ZGADZAJA - TRUE
				ResultSet rs = stmt.executeQuery();
				if (rs.next()) 
				{
					if(opened)
					{
						//ZWIEKSZENIE ILOSCI UZYTKOWNIKOW
						usercount++;
						serverframe.connectstatus.setText("Liczba pod��czonych u�ytkowni�w: " + usercount);
						serverframe.log.append("Klient: Zalogowano \"" + l + "\".\n");
					}
					return true;
				}
				else{
					return false;
				}
			}
			else{
				return false;
			}
		} catch (SQLException e) {
			System.out.println("sqlexcep");
			return false;
		}
	}
	@Override
	public boolean checkOnline() throws RemoteException {
		return opened;
	}
	
	@Override
	public void finishedimporting() throws RemoteException {
		importer = "empty";
		importingnum = 0;
	}
	@Override
	public String importNote(String login) throws RemoteException {
		int num = 0;
	    while (userslist.size() > num) {
	    	  if(userslist.get(num).login.equals(login)){
	    		  String note =  userslist.get(num).notes.get(importingnum);
	    		  importingnum++;
	    		  return note;
	    	  }
	    	  num++;
	    }
		return "null";
	}

	
	@Override
	public boolean canexport(String login) throws RemoteException {
		if(importer.equals("empty"))
		{
			importingnum=0;
			importer = login;
			return true;
		}
		else return false;
	}
	
	@Override
	public int howmanynotes(String login) throws RemoteException {
		int num = 0;
	    while (userslist.size() > num) {
	    	  if(userslist.get(num).login.equals(login))
	    		  return userslist.get(num).notes.size();
	    	  num++;
	    }
	    return 0;
	}

	@Override
	public void addNote(String login, int newlastid, String note) throws RemoteException {
		lastid = newlastid;
		serverframe.log.append("Klient: \""+ login + "\" doda� notatke id: " + lastid + ".\n");
		try {
			StringTokenizer stringTokenizer = new StringTokenizer(note, ".");
			stringTokenizer.nextToken();
			String sql = "INSERT INTO Notes(ID, Owner, Day, Month, Year, Hour, Minute, Text) VALUES (NULL, '"+ login + "', '" + stringTokenizer.nextToken() + "', '" +stringTokenizer.nextToken() + "', '" + stringTokenizer.nextToken() + "', '" + stringTokenizer.nextToken() + "', '"+stringTokenizer.nextToken() +"', '"+ (stringTokenizer.countTokens() > 0 ? stringTokenizer.nextToken() : "") +"')";
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			System.out.println("sqlexcep przy dodawaniu notatki");
		}
	}
	
	@Override
	public void logout(String l) throws RemoteException {	
		serverframe.log.append("Klient: Wylogowano \"" + l + "\".\n");
		usercount--;
		serverframe.connectstatus.setText("Liczba pod��czonych u�ytkowni�w: " + usercount);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//OTWIERANIE SERWERA
		if (!opened) {
			serv.openServer();
			serverframe.connectstatus.setText("Oczekiwanie na u�ytkownik�w...");
			serverframe.connectbutton.setText("Zamknij serwer");
		}
		else{
		//ZAMYKANIE SERWERA
			serv.closeServer();
			serverframe.connectbutton.setText("Uruchom serwer");
			serverframe.connectstatus.setText("Kliknij, �eby czeka� na po��czenie");
		}
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		closeServer();
		try {
			if(connectedtobd){
				if(connectedclient)
					serv.stmt.close();
				serv.con.close();
			}
		} catch (SQLException e2) {
			serv.serverframe.connectstatus.setText("B��d w zamykaniu bazy danych.");
		}
		while(usercount>0)
			try {
				Thread.sleep(200);
			} catch (InterruptedException e1) {
				System.out.println("B�ad sleep()");
			}
		opened=false;
	}
	
	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public int lastid() throws RemoteException {
		return lastid;
	}

	@Override
	public void editNote(String login, String note) throws RemoteException {
		try {
			StringTokenizer stringTokenizer = new StringTokenizer(note, ".");
			int id = Integer.valueOf(stringTokenizer.nextToken());
			int day = Integer.valueOf(stringTokenizer.nextToken());
			int month = Integer.valueOf(stringTokenizer.nextToken());
			int year = Integer.valueOf(stringTokenizer.nextToken());
			int hour = Integer.valueOf(stringTokenizer.nextToken());
			int minute = Integer.valueOf(stringTokenizer.nextToken());
			String text = stringTokenizer.nextToken();
			String sql = "UPDATE Notes SET Day = " + day + ", Month = " + month + ", Year = " + year + ", Hour = " + hour + ", Minute = " + minute + ", Text = '" + text + "' WHERE ID = " + id + ";";
			stmt.executeUpdate(sql);
			serverframe.log.append("Klient: \"" + login + "\" edytowa� notatke id: " + id + ".\n");
		} catch (SQLException e) {
			serverframe.log.append("B��d bazy danych.\n");
		}

	}

	@Override
	public void deleteNote(int id, String login) throws RemoteException {
		try {
			String sql = "DELETE FROM Notes WHERE ID = " + id + ";";
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			serverframe.log.append("B��d bazy danych.\n");
		}
		serverframe.log.append("Klient: \"" + login + "\" usun�� notatke id: " + id + ".\n");
	}

	@Override
	public void refreshUser(String login) throws RemoteException {
		int num = 0;
	    while (userslist.size() > num) {
	    	 if(login.equals(userslist.get(num).login)){
	    		 userslist.get(num).refresh(con,stmt);
	    	 }
	    	  num++;
	    }
	}
}
