package serverorg;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class ServerFrame extends JFrame{
	public JButton connectbutton;
	public JLabel connectstatus;
	public JTextArea log;
	public JScrollPane scrollpane;

	
	private void Init(){
		setTitle("Serwer");
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(360,320);
		setResizable(false);
		setLocation(660,150);
		setVisible(true);
		log = new JTextArea();
		log.setBounds(30,100,300,180);
		log.setLineWrap(true);
		log.setWrapStyleWord(true);
		scrollpane= new JScrollPane(log);
		scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollpane.setBounds(30,100,300,180);
		add(scrollpane);
		connectbutton = new JButton("Uruchom serwer");
		connectbutton.setBounds(105,20,150,40);
		add(connectbutton);
		connectstatus = new JLabel("Kliknij, �eby czeka� na po��czenie");
		connectstatus.setBounds(90,60,220,20);
		add(connectstatus);
		repaint();
	}
	
	ServerFrame(){
		Init();
	}
}
