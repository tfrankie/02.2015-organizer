package ServerIntf;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;



public interface ServIntf extends Remote{
	public boolean login(String l, String p) throws RemoteException;
	public void logout(String l) throws RemoteException;
	public boolean isOpen() throws RemoteException;
	public boolean checkOnline() throws RemoteException;
	public void addNote(String login, int newlastid, String note) throws RemoteException;
	public String importNote(String login) throws RemoteException;
	public int howmanynotes(String login) throws RemoteException;
	public boolean canexport(String login) throws RemoteException;
	public void finishedimporting() throws RemoteException;
	public int lastid() throws RemoteException;
	public void editNote(String login, String note) throws RemoteException;
	public void deleteNote(int id, String login) throws RemoteException;
	public void refreshUser(String login) throws RemoteException;
}
